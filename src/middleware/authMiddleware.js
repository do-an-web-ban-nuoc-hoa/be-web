import jwt from "jsonwebtoken";
import dotenv from "dotenv";
dotenv.config();
const authMiddleWare = (req, res, next) => {
  const token = req.headers.token.split(" ")[1];
  console.log(token);
  jwt.verify(token, process.env.ACCESS_TOKEN, function (err, user) {
    if (err) {
      res.status(200).json({
        message: err,
      });
    }
    if (user) {
      const { id, isAdmin } = user.payload;
      if (isAdmin) {
        next();
      } else {
        return res.status(404).json({
          message: "The role is not allowed",
        });
      }
    }
  });
};
export { authMiddleWare };
