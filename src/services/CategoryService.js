import Category from "../models/CategorySchema.js";
import Product from "../models/ProductModel.js";
import { v2 as cloudinary } from "cloudinary";

cloudinary.config({
  cloud_name: "dd05dvci1",
  api_key: "972422196116553",
  api_secret: "zdrq30Hlao_KDQPCXU1hlKLNlLo",
});
const createCategory = async (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      const { name, parent, image_url } = data;
      await cloudinary.uploader
        .upload(`data:image/jpeg;base64,${image_url}`, {
          overwrite: true,
          invalidate: true,
          width: 331,
          height: 316,
          crop: "fill",
          upload_preset: "ml_default",
          resource_type: "image",
        })
        .then(async (result) => {
          const createdCategory = await Category.create({
            name: name,
            parent: parent,
            image_url: result.url,
          });
          resolve({
            status: "Okay",
            message: "Create Category successfully!!!",
            data: createdCategory,
          });
        });
    } catch (error) {
      reject(error);
    }
  });
};
const getDetailCategory = async (categoryID) => {
  return new Promise(async (resolve, reject) => {
    try {
      const productOfCategory = await Product.find({
        category: categoryID,
      });
      console.log(productOfCategory);
      const category = await Category.findOne({
        _id: categoryID,
      });

      resolve({
        status: "Okay",
        message: "Get detail category successfully!!!",
        data: category,
      });
    } catch (error) {
      reject(error);
    }
  });
};
const getAllCategory = async () => {
  return new Promise(async (resolve, reject) => {
    try {
      const category = await Category.find();
      resolve({
        status: "Okay",
        message: "Get all category successfully!!!",
        data: category,
      });
    } catch (error) {
      reject(error);
    }
  });
};
const deleteCategory = async (categoryID) => {
  return new Promise(async (resolve, reject) => {
    try {
      console.log(categoryID);
      await Category.findByIdAndDelete(categoryID);
      resolve({
        status: "Okay",
        message: "Delete category successfully!!!",
      });
    } catch (error) {
      reject(error);
    }
  });
};
const updateCategory = async (categoryID, data) => {
  return new Promise(async (resolve, reject) => {
    try {
      const currentCategory = await Category.findById({
        _id: categoryID,
      });
      const oldImage = currentCategory.image_url;

      if (oldImage) {
        const start = oldImage.lastIndexOf("/");
        const end = oldImage.lastIndexOf(".");
        const public_id = oldImage.substring(start + 1, end);
        await cloudinary.uploader.destroy(public_id);
        await cloudinary.uploader
          .upload(`data:image/jpeg;base64,${data.image_url}`, {
            overwrite: true,
            invalidate: true,
            width: 331,
            height: 316,
            crop: "fill",
            upload_preset: "ml_default",
            resource_type: "image",
          })
          .then(async (result) => {
            const categoryUpdated = Category.findByIdAndUpdate(
              categoryID,
              { ...data, image_url: result.url },
              { new: true }
            );
            resolve({
              status: "Okay",
              message: "Updated successfully!!!",
              data: categoryUpdated,
            });
          });
      }
      await cloudinary.uploader
        .upload(`data:image/jpeg;base64,${data.image_url}`, {
          overwrite: true,
          invalidate: true,
          width: 331,
          height: 316,
          crop: "fill",
          upload_preset: "ml_default",
          resource_type: "image",
        })
        .then(async (result) => {
          const updatedCategory = await Category.findByIdAndUpdate(
            categoryID,
            {
              ...data,
              image_url: result.url,
            },
            { new: true }
          );
          resolve({
            status: "Okay",
            message: "Updated category successfully!!!",
            data: updatedCategory,
          });
        });
    } catch (error) {
      reject(error);
    }
  });
};
export {
  createCategory,
  getDetailCategory,
  getAllCategory,
  deleteCategory,
  updateCategory,
};
