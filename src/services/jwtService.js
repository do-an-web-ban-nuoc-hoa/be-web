import jwt from "jsonwebtoken";
import dotenv from "dotenv";
dotenv.config();
export const generalAccessToken = async (payload) => {
  const access_token = jwt.sign(
    {
      payload,
    },
    process.env.ACCESS_TOKEN,
    { expiresIn: "2d" }
  );
  return access_token;
};
export const generalRefreshToken = async (payload) => {
  const refresh_token = jwt.sign(
    {
      payload,
    },
    process.env.REFRESH_TOKEN,
    { expiresIn: "365d" }
  );
  return refresh_token;
};
export const refreshTokenService = async (token) => {
  return new Promise(async (resolve, reject) => {
    console.log(token);
    try {
      jwt.verify(token, process.env.REFRESH_TOKEN, async (err, user) => {
        if (err) {
          resolve({
            status: "Err",
            message: err,
          });
        }
        console.log(user);
        const access_token = await generalAccessToken({
          _id: user?._id,
          isAdmin: user?.isAdmin,
        });
        resolve({
          status: "Ok",
          message: "Refesh token successfully",
          access_token,
        });
      });
    } catch (error) {
      reject(error);
    }
  });
};
