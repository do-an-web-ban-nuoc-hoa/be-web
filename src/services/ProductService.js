import Product from "../models/ProductModel.js";
import { v2 as cloudinary } from "cloudinary";
import csv from "csvtojson";
import dotenv from "dotenv";
dotenv.config();
cloudinary.config({
  cloud_name: process.env.CLOUD_NAME,
  api_key: process.env.API_KEY,
  api_secret: process.env.API_SECRET,
});
const createProduct = async (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      console.log("Vao service");
      const { name, image, price, stock, rating, type, description, category } =
        data;
      if (image) {
        await cloudinary.uploader
          .upload(`data:image/jpeg;base64,${image}`, {
            overwrite: true,
            invalidate: true,
            width: 810,
            height: 456,
            crop: "fill",
            upload_preset: "ml_default",
            resource_type: "image",
          })
          .then(async (result) => {
            console.log(result);
            const createdProduct = await Product.create({
              name,
              image: result.url,
              price,
              stock,
              rating,
              type,
              description,
              category,
            });

            resolve({
              status: "Okay!",
              message: "Create successfull!",
              data: createdProduct,
            });
          });
      }
      const createdProduct = await Product.create({
        name,
        price,
        stock,
        rating,
        type,
        description,
        category,
      });
      resolve({
        status: "Okay!",
        message: "Created test okay",
        data: createdProduct,
      });
    } catch (error) {
      reject(error);
    }
  });
};
const getAllProduct = async (
  page,
  limit,
  sort,
  search,
  type,
  category,
  from,
  to,
  name,
  brand
) => {
  return new Promise(async (resolve, reject) => {
    try {
      const totalProduct = await Product.count();
      if (sort) {
        let objectSort = {};
        objectSort[sort[1]] = sort[0];
        console.log(objectSort);
        const allProductSort = await Product.find()
          .limit(limit)
          .skip(limit * page)
          .sort(objectSort);
        resolve({
          status: "Okay",
          message: "Get all products successfully",
          data: allProductSort,
          total: totalProduct,
          currentPage: Number(page + 1),
          totalPage: Math.ceil(totalProduct / limit),
        });
      }
      if (type || category || from || to || name || brand) {
        let newType = type;
        if (type !== undefined) {
          newType = type.split(",");
        }
        const dataFilter = [
          { type: { $in: newType } },
          { category },
          { price: { $gte: from, $lte: to } },
          { name: { $regex: name, $options: "i" } },
          {
            brand: { $in: brand },
          },
        ];
        let lastFilter = [];
        dataFilter.forEach((x, index) => {
          if (Object.values(x).includes(undefined)) dataFilter.splice(1, index);
          else {
            console.log(Object.values(...Object.values(x)));
            if (!Object.values(...Object.values(x)).includes(undefined))
              lastFilter.push(x);
          }
        });
        console.log(lastFilter);
        const query = {};
        lastFilter.forEach((filter) => {
          Object.assign(query, filter);
        });
        console.log(query);
        const allProductFilter = await Product.find({ ...query })
          .limit(limit)
          .skip(limit * page);
        const totalProductFilter = await Product.find({ ...query }).count();
        console.log(totalProductFilter);
        resolve({
          status: "Okay",
          message: "Get all products successfully",
          data: allProductFilter,
          total: totalProductFilter,
          currentPage: Number(page + 1),
          totalPage: Math.ceil(totalProductFilter / limit),
        });
      }
      const allProduct = await Product.find()
        .limit(limit)
        .skip(limit * page);
      resolve({
        status: "Okay",
        message: "Get all products successfully",
        data: allProduct,
        total: totalProduct,
        currentPage: Number(page + 1),
        totalPage: Math.ceil(totalProduct / limit),
        totalProduct: totalProduct,
      });
    } catch (error) {
      reject(error);
    }
  });
};
const getDetailProduct = async (productID) => {
  return new Promise(async (resolve, reject) => {
    try {
      const productDetail = await Product.findOne({
        _id: productID,
      });
      console.log(productDetail);
      resolve({
        status: "Okay",
        message: "Get product detail successfull!!!",
        data: productDetail,
      });
    } catch (error) {
      reject(error);
    }
  });
};
const updateProduct = async (productID, newData) => {
  return new Promise(async (resolve, reject) => {
    try {
      console.log("vao service");
      const data = await Product.findById({
        _id: productID,
      });
      const oldImage = data.image;
      const { name, image, price, stock, rating, description, category } =
        newData;
      if (image) {
        const start = oldImage.lastIndexOf("/");
        const end = oldImage.lastIndexOf(".");
        const public_id = oldImage.substring(start + 1, end);
        await cloudinary.uploader.destroy(public_id);
        await cloudinary.uploader
          .upload(`data:image/jpeg;base64,${image}`, {
            overwrite: true,
            invalidate: true,
            width: 810,
            height: 456,
            crop: "fill",
            upload_preset: "ml_default",
            resource_type: "image",
          })
          .then(async (result) => {
            const updatedData = await Product.findByIdAndUpdate(
              productID,
              {
                name,
                image: result.url,
                price,
                stock,
                rating,
                description,
                category,
              },
              { new: true }
            );
            console.log(updatedData);
            resolve({
              status: "Okay",
              message: "updated successfully",
              data: updatedData,
            });
          });
      }
      const updatedData = await Product.findByIdAndUpdate(
        productID,
        {
          name,
          price,
          stock,
          rating,
          description,
          category,
        },
        { new: true }
      );
      resolve({
        status: "Okay",
        message: "updated successfully",
        data: updatedData,
      });
    } catch (error) {
      reject(error);
    }
  });
};
const deleteProduct = async (productID) => {
  return new Promise(async (resolve, reject) => {
    try {
      const currentProduct = await Product.findById({
        _id: productID,
      });
      const oldImage = currentProduct.image;
      const start = oldImage.lastIndexOf("/");
      const end = oldImage.lastIndexOf(".");
      const public_id = oldImage.substring(start + 1, end);
      await cloudinary.uploader.destroy(public_id);
      await Product.findByIdAndDelete(productID);
      resolve({
        status: "Okay",
        message: "Delete successfully",
      });
    } catch (error) {
      reject(error);
    }
  });
};
const deleteAllProduct = async () => {
  return new Promise(async (resolve, reject) => {
    try {
      await Product.deleteMany({});
      resolve({
        status: "Okay",
        message: "Delete All Successfully!!!",
      });
    } catch (error) {
      reject(error);
    }
  });
};
const importProducts = async (path) => {
  return new Promise(async (resolve, reject) => {
    try {
      csv()
        .fromFile(path)
        .then(async (response) => {
          let newResposne = await Promise.all(
            response.map(async (item) => {
              try {
                await cloudinary.uploader
                  .upload(item.image, {
                    overwrite: true,
                    invalidate: true,
                    width: 241,
                    height: 250,
                    crop: "fill",
                    upload_preset: "ml_default",
                    resource_type: "image",
                  })
                  .then((result) => {
                    item.image = result.url;
                  });
                return item;
              } catch (error) {
                console.log(error);
              }
            })
          );
          console.log(newResposne);
          const importProducts = await Product.insertMany(newResposne);
          resolve({
            status: "Okay",
            message: "Imported okay",
            data: importProducts,
          });
        });
    } catch (error) {
      reject(error);
    }
  });
};
export {
  createProduct,
  getAllProduct,
  getDetailProduct,
  updateProduct,
  deleteProduct,
  deleteAllProduct,
  importProducts,
};
