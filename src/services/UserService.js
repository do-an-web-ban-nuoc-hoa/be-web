import User from "../models/UserModel.js";
import bcrypt from "bcrypt";
import { generalAccessToken, generalRefreshToken } from "./jwtService.js";
const createUser = (newUser) => {
  return new Promise(async (resolve, reject) => {
    const { name, email, password, confirmPassword, phone } = newUser;
    try {
      const hash = bcrypt.hashSync(password, 10);
      const createdUser = await User.create({
        name,
        email,
        password: hash,
        phone,
      });
      if (createdUser) {
        resolve({
          status: "OK",
          message: "SUCCESS",
          data: createdUser,
        });
      }
    } catch (error) {
      reject(error);
    }
  });
};
const loginUser = (loginUser) => {
  return new Promise(async (resolve, reject) => {
    const { name, email, password, phone } = loginUser;
    try {
      const checker = await User.findOne({
        email: email,
      });
      if (checker === null) {
        resolve({
          status: "ERR",
          message: "Tài khoản không tồn tại",
        });
      }

      const comparePassword = bcrypt.compare(password, checker.password);
      if (!comparePassword) {
        resolve({
          status: "ERR",
          message: "Mật khẩu không chính xác",
        });
      }
      const access_token = await generalAccessToken({
        id: checker.id,
        isAdmin: checker.isAdmin,
      });
      const refresh_token = await generalRefreshToken({
        id: checker.id,
        isAdmin: checker.isAdmin,
      });
      // if (createdUser) {
      resolve({
        status: "OK",
        message: "SUCCESS",
        access_token,
        refresh_token,
      });
      // }
    } catch (error) {
      reject(error);
    }
  });
};
const updateUser = (userID, data) => {
  return new Promise(async (resolve, reject) => {
    try {
      const checkUser = await User.findOne({
        _id: userID,
      });
      const updatedUser = await User.findByIdAndUpdate(userID, data, {
        new: true,
      });
      console.log(updatedUser);
      resolve({
        status: "OK",
        message: "SUCCESS",
        data: updatedUser,
      });
      console.log(checkUser);
    } catch (error) {
      reject(error);
    }
  });
};
const deleteUser = (userID) => {
  return new Promise(async (resolve, reject) => {
    try {
      const checkerUser = User.findOne({
        _id: userID,
      });
      await User.findByIdAndDelete(userID);
      resolve({
        status: "OK",
        message: "Delete successfull",
      });
    } catch (error) {
      reject(error);
    }
  });
};
const getAllUser = () => {
  return new Promise(async (resolve, reject) => {
    try {
      const allUser = await User.find();
      resolve({
        status: "OK",
        message: "get user successfull",
        data: allUser,
      });
    } catch (error) {
      reject(error);
    }
  });
};
const getDetailUser = (userID) => {
  return new Promise(async (resolve, reject) => {
    try {
      const userChecker = await User.findById({
        _id: userID,
      }).populate("cart");

      if (userChecker) {
        resolve({
          status: "OK",
          message: "Get Successfull",
          data: userChecker,
        });
      }
    } catch (error) {
      reject(error);
    }
  });
};

export {
  createUser,
  loginUser,
  updateUser,
  deleteUser,
  getAllUser,
  getDetailUser,
};
