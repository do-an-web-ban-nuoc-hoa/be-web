import Cart from "../models/CartSchema.js";

const createCart = async (data) => {
  const { user, orderItem, totalPrice } = data;
  console.log("user", user);
  console.log("orderItem", orderItem);
  console.log("total", totalPrice);
  return new Promise(async (resolve, reject) => {
    try {
      const createdCart = await Cart.create({
        user,
        orderItem,
        totalPrice,
      });
      resolve({
        message: "Okay",
        status: "200",
        data: createdCart,
      });
    } catch (error) {
      reject(error);
    }
  });
};
const updateCart = async (data, cartID) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (data?.orderItem) {
        console.log(data?.totalPrice);
        await Cart.updateOne(
          {
            _id: cartID,
          },
          {
            $push: {
              orderItem: data.orderItem,
            },
            $set: { totalPrice: data?.totalPrice },
          }
        );
        const updatedCart = await Cart.findById({
          _id: cartID,
        });
        resolve({
          status: "Okay",
          message: "update cart successfully",
          data: updatedCart,
        });
      }
      if (data?.updateQuantity) {
        const elementUpdate = data?.updateQuantity;
        console.log(data?.totalPrice);
        await Cart.updateOne(
          {
            _id: cartID,
            "orderItem.product": elementUpdate.product,
          },
          {
            $set: {
              "orderItem.$.quantity": elementUpdate.quantity,
              totalPrice: data?.totalPrice,
            },
          },
          {}
        );
        const updatedCart = await Cart.findById({
          _id: cartID,
        });
        resolve({
          status: "Okay",
          message: "update cart successfully",
          data: updatedCart,
        });
      }
    } catch (error) {
      reject(error);
    }
  });
};
const deleteCart = async (cartID) => {
  return new Promise(async (resolve, reject) => {
    try {
      await Cart.updateOne(
        {
          _id: cartID,
        },
        {
          $set: { orderItem: [] },
          totalPrice: 0,
        }
      );
      resolve({
        status: "Okay",
        message: "Delelte all successfully!!!",
        data: "test",
      });
    } catch (error) {
      reject(error);
    }
  });
};
const deleteCartDetail = async (cartID, data) => {
  return new Promise(async (resolve, reject) => {
    try {
      await Cart.updateOne(
        {
          _id: cartID,
        },
        {
          $pull: { orderItem: { _id: data.productId } },
          totalPrice: data.totalPrice,
        }
      );
      resolve({
        status: "Okay",
        message: "Delete this Item successfully!",
      });
    } catch (error) {
      reject(error);
    }
  });
};
const getCartByUser = async (userID) => {
  return new Promise(async (resolve, reject) => {
    try {
      const cartGetByUser = await Cart.find().populate("user");
      resolve({
        status: "Okay",
        message: "Get cart successfully",
        data: cartGetByUser[0],
      });
    } catch (error) {
      reject(error);
    }
  });
};
export { createCart, updateCart, deleteCart, getCartByUser, deleteCartDetail };
