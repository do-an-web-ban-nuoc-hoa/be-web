import Order from "../models/OrderProduct.js";
import dateFormat, { masks } from "dateformat";
import QueryString from "qs";
import crypto from "crypto";
const createOrder = async (data, ipAddr) => {
  return new Promise(async (resolve, reject) => {
    try {
      let date = new Date();
      const createdOrder = await Order.create(data);
      let secretKey = process.env.VNP_HASHSECRET;
      let vnpUrl = process.env.VNP_URL;
      let returnUrl = process.env.VNP_RETURN_URL;
      let tmnCode = process.env.VNP_CODE;
      console.log(returnUrl);
      let vnpParams = {
        vnp_Version: "2.1.0",
        vnp_Command: "pay",
        vnp_CreateDate: dateFormat(date, "yyyymmddHHMMss"),
        vnp_TxnRef: dateFormat(date, "HHMMss"),
        vnp_Amount: data.totalPrice * 100,
        vnp_OrderInfo: "123",
        vnp_OrderType: "210000",
        vnp_Locale: "vn",
        vnp_CurrCode: "VND",
        vnp_TmnCode: tmnCode,
        vnp_IpAddr: ipAddr,
        vnp_ReturnUrl: returnUrl,
      };

      vnpParams = sortObject(vnpParams);
      let querystring = QueryString;
      let signData = querystring.stringify(vnpParams, { encode: false });

      let hmac = crypto.createHmac("sha512", secretKey);
      let signed = hmac.update(Buffer.from(signData, "utf-8")).digest("hex");
      vnpParams["vnp_SecureHash"] = signed;
      vnpUrl += "?" + querystring.stringify(vnpParams, { encode: false });
      console.log(vnpUrl);
      resolve({
        status: "Okay",
        message: "Create order successfully!",
        data: createdOrder,
        return_url: vnpUrl,
      });
    } catch (error) {
      reject(error);
    }
  });
};

function sortObject(obj) {
  let sorted = {};
  let str = [];
  let key;
  for (key in obj) {
    if (obj.hasOwnProperty(key)) {
      str.push(encodeURIComponent(key));
    }
  }
  str.sort();
  for (key = 0; key < str.length; key++) {
    sorted[str[key]] = encodeURIComponent(obj[str[key]]).replace(/%20/g, "+");
  }
  return sorted;
}

export { createOrder };
