import Banner from "../models/BannerSchema.js";
import { v2 as cloudinary } from "cloudinary";

cloudinary.config({
  cloud_name: process.env.CLOUD_NAME,
  api_key: process.env.API_KEY,
  api_secret: process.env.API_SECRET,
});
const getAllBanner = async () => {
  return new Promise(async (resolve, reject) => {
    try {
      const allBanner = await Banner.find();
      resolve({
        status: "Okay",
        message: "Get All Banner successfully!!!",
        data: allBanner,
      });
    } catch (error) {
      reject(error);
    }
  });
};
const createBanner = async (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      const { image_url, type } = data;
      const uploader = await cloudinary.uploader.upload(
        `data:image/jpeg;base64,${image_url}`,
        {
          overwrite: true,
          invalidate: true,
          width: 1024,
          height: 350,
          crop: "fill",
          upload_preset: "ml_default",
          resource_type: "image",
        }
      );
      const bannerCreated = await Banner.create({
        image_url: uploader.url,
        type,
      });
      resolve({
        status: "Okay",
        message: "test",
        data: bannerCreated,
      });
    } catch (error) {
      reject(error);
    }
  });
};
export { getAllBanner, createBanner };
