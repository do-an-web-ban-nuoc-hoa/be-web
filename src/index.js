import express from "express";
import dotenv from "dotenv";
import mongoose from "mongoose";
import routes from "./routers/index.js";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import cors from "cors";
dotenv.config();
const app = express();
const port = process.env.PORT || 9000;
app.use(cors());
app.use(bodyParser.json({ limit: "50mb" }));
app.use(express.json({ limit: "50mb" }));
app.use(
  express.urlencoded({ limit: "50mb", parameterLimit: 100000, extended: true })
);
app.use(cookieParser());
app.use(
  bodyParser.urlencoded({
    limit: "50mb",
    extended: true,
    parameterLimit: 100000,
  })
);
routes(app);

app.get("/", (req, res) => {
  res.send("123");
});
mongoose
  .connect(
    "mongodb+srv://nguyenhuuduylam05:UliCaEl9WsOnD32U@cluster0.fcjmlkt.mongodb.net/?retryWrites=true&w=majority"
  )
  .then(() => {
    console.log("Connect Db success");
  })
  .catch((err) => {
    console.log(err);
  });
app.listen(port, () => {
  console.log("Server is running in port:" + port);
});
