import mongoose from "mongoose";
const orderSchema = mongoose.Schema({
  orderItem: [
    {
      name: { type: String, require: true },
      quantity: { type: Number, require: true },
      image: { type: String, require: true },
      price: { type: String, require: true },
      product: {
        type: mongoose.Schema.ObjectId,
        ref: "Product",
        require: true,
      },
    },
  ],
  shippingAddress: {
    fullName: { type: String, require: true },
    address: { type: String, require: true },
    city: { type: String, require: true },
    country: { type: String, require: true },
    phone: { type: Number, require: true },
  },
  paymentMethod: { type: String, require: true },
  itemsPrice: { type: Number, require: true },
  shippingPrice: { type: Number, require: true },
  totalPrice: { type: Number, require: true },
  user: { type: mongoose.Schema.ObjectId, ref: "User", require: true },
  isPaid: { type: Boolean, default: false },
  paidAt: { type: Date },
  isDeliveredAt: { type: Boolean, default: false },
  deliveredAt: { type: Date },
});
const Order = mongoose.model("Order", orderSchema);
export default Order;
