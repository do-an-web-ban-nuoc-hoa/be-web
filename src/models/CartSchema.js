import mongoose, { Model, Schema } from "mongoose";

const CartSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.ObjectId,
    ref: "User",
    require: true,
  },
  orderItem: [
    {
      quantity: { type: Number, require: true },
      product: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Product",
        require: true,
      },
      price: { type: Number, require: true },
      name: { type: String, require: true },
      image: { type: String, require: true },
    },
  ],

  totalPrice: { type: Number, require: true },
});

const Cart = mongoose.model("Cart", CartSchema);
export default Cart;
