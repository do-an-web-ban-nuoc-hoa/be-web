import mongoose from "mongoose";
const productSchema = new mongoose.Schema(
  {
    name: { type: String, require: true },
    image: { type: String, require: true },
    price: { type: Number, require: true },
    stock: { type: Boolean, default: false, require: true },
    rating: { type: Number, require: true },
    type: { type: String, require: true },
    description: { type: String, require: true },
    category: {
      type: mongoose.Schema.ObjectId,
      ref: "Category",
      require: true,
    },
  },
  {
    timestamps: true,
  }
);
const Product = mongoose.model("Product", productSchema);
export default Product;
