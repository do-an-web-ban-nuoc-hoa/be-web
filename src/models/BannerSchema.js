import mongoose from "mongoose";

const bannerSchema = new mongoose.Schema({
  image_url: { type: String, require: true },
  type: { type: String, require: true },
});

const Banner = mongoose.model("Banner", bannerSchema);

export default Banner;
