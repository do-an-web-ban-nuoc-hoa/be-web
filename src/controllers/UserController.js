import {
  createUser,
  loginUser,
  updateUser,
  deleteUser,
  getAllUser,
  getDetailUser,
} from "../services/UserService.js";
import { refreshTokenService } from "../services/jwtService.js";
const createUserController = async (req, res) => {
  try {
    console.log(req.body);
    const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    const { name, email, password, confirmPassword, phone } = req.body;
    const isValidEmail = reg.test(email);

    if (!name || !email || !password || !phone) {
      return res.status(200).json({
        status: "error",
        message: "The input is required",
      });
    } else if (!isValidEmail) {
      return res.status(200).json({
        status: "error",
        message: "The email is not valid",
      });
    }
    const response = await createUser(req.body);

    return res.status(200).json(response);
  } catch (error) {
    return res.status(404).json({
      message: error,
    }); //
  }
};
const loginUserController = async (req, res) => {
  try {
    console.log(req.body);
    const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    const { name, email, password, confirmPassword, phone } = req.body;
    const isValidEmail = reg.test(email);

    const response = await loginUser(req.body);
    const { refresh_token, ...newResponse } = response;
    res.cookie("refresh_token", refresh_token, {
      HttpOnly: true, //CHỉ lấy dc từ http chứ ko lấy dc bằng js
      // Secure: falses,
      // samesite: "strict", //Bảo mật
    });
    return res.status(200).json(newResponse);
  } catch (error) {
    return res.status(404).json({
      message: error,
    }); //
  }
};
const updateUserController = async (req, res) => {
  try {
    const userId = req.params.id;
    console.log("userID", userId);
    const data = req.body;
    if (!userId) {
      return res.status(200).json({
        message: "not have userID",
      });
    }
    const response = await updateUser(userId, data);
    return res.status(200).json(response);
  } catch (error) {
    return res.status(404).json({
      message: error,
    });
  }
};
const deleteUserController = async (req, res) => {
  try {
    const userID = req.params.id;
    console.log("đã qua");
    const response = await deleteUser(userID);
    return res.status(200).json(response);
  } catch (error) {
    return res.status(200).json({
      message: error,
    });
  }
};
const getAllUserController = async (req, res) => {
  try {
    const response = await getAllUser();
    return res.status(200).json(response);
  } catch (error) {
    res.status(200).json(error);
  }
};
const getDetailUserController = async (req, res) => {
  try {
    const userID = req.params.id;
    const response = await getDetailUser(userID);
    return res.status(200).json(response);
  } catch (error) {
    res.status(200).json(error);
  }
};
const refreshTokenController = async (req, res) => {
  try {
    console.log("refresh_token", req.cookies);
    const token = req.cookies.refresh_token;
    console.log(token);
    if (!token) {
      return res.status(200).json({
        status: "Err",
        message: "The token is required",
      });
    }
    const response = await refreshTokenService(token);
    return res.status(200).json(response);
  } catch (error) {
    return res.status(200).json(error);
  }
};
export {
  createUserController,
  loginUserController,
  updateUserController,
  deleteUserController,
  getAllUserController,
  getDetailUserController,
  refreshTokenController,
};
