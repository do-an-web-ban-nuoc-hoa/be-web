import {
  createProduct,
  getAllProduct,
  getDetailProduct,
  updateProduct,
  deleteProduct,
  deleteAllProduct,
  importProducts,
} from "../services/ProductService.js";
const createProductController = async (req, res) => {
  try {
    console.log("vào controller");
    const data = req.body.filter((x) => x?.category?._id !== null);
    console.log(data);
    // const { name, image, price, stock, rating, description, category } = data;
    // if (!name || !image || !price || !stock || !category) {
    //   return res.status(401).json({
    //     message: "The field is required",
    //   });
    // }
    const response = await createProduct(data);
    return res.status(200).json(response);
  } catch (error) {
    return res.status(401).json({
      message: error,
    });
  }
};
const getAllProductController = async (req, res) => {
  try {
    const { page, limit, sort, search, type, category, from, to, name, brand } =
      req.query;
    const response = await getAllProduct(
      Number(page) || 0,
      Number(limit) || 8,
      sort,
      search,
      type,
      category,
      from,
      to,
      name,
      brand
    );
    return res.status(200).json(response);
  } catch (error) {
    return res.status(401).json({
      message: error,
    });
  }
};
const getDetailProductController = async (req, res) => {
  try {
    const productID = req.params.id;
    const response = await getDetailProduct(productID);
    return res.status(200).json(response);
  } catch (error) {
    return res.status(401).json(error);
  }
};
const updateProductController = async (req, res) => {
  try {
    console.log("Vao controller");
    const productID = req.params.id;
    const data = req.body;
    const response = await updateProduct(productID, data);
    return res.status(200).json(response);
  } catch (err) {
    return res.status(401).json(err);
  }
};
const deleteProductController = async (req, res) => {
  try {
    const productID = req.params.id;
    const response = await deleteProduct(productID);
    return res.status(200).json(response);
  } catch (err) {
    return res.status(401).json(err);
  }
};
const deleteAllProductController = async (req, res) => {
  try {
    const response = await deleteAllProduct();
    return res.status(200).json(response);
  } catch (error) {
    return res.status(401).json({
      message: error,
    });
  }
};
const ProductImportController = async (req, res) => {
  try {
    const path = req.file.path;
    const response = await importProducts(path);
    return res.status(200).json(response);
  } catch (error) {
    return res.status(200).json({
      msg: error,
    });
  }
};
export {
  createProductController,
  getAllProductController,
  getDetailProductController,
  updateProductController,
  deleteProductController,
  deleteAllProductController,
  ProductImportController,
};
