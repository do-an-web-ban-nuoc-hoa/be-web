import { createOrder } from "../services/OrderService.js";

const createOrderController = async (req, res) => {
  try {
    const ipAddr = req.socket.localAddress;
    //   req.headers["x-forwarded-for"] ||
    //   req.connection.remoteAddress ||
    //   req.socket.remoteAddress ||
    //   req.connection.socket.remoteAddress;

    const data = req.body;
    const response = await createOrder(data, ipAddr);
    return res.status(200).json(response);
  } catch (error) {
    return res.status(401).json(error);
  }
};
const getReturnUrlOrderController = async (req, res) => {
  console.log(req.query);
  try {
    return res
      .status(301)
      .redirect("https://tonnie05.vercel.app/payment/success");
  } catch (error) {}
};
export { createOrderController, getReturnUrlOrderController };
