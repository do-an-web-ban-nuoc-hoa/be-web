import { createBanner, getAllBanner } from "../services/BannerService.js";

const getAllBannerController = async (req, res) => {
  try {
    const response = await getAllBanner();
    return res.status(200).json({
      response,
    });
  } catch (error) {
    return res.status(401).json({
      message: error,
    });
  }
};
const createBannerController = async (req, res) => {
  try {
    const data = req.body;
    console.log(data);
    const response = await createBanner(data);
    return res.status(200).json(response);
  } catch (error) {
    return res.status(401).json(error);
  }
};
export { getAllBannerController, createBannerController };
