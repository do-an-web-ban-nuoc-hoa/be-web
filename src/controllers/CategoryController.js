import {
  createCategory,
  getDetailCategory,
  getAllCategory,
  deleteCategory,
  updateCategory,
} from "../services/CategoryService.js";

const createCategoryController = async (req, res) => {
  try {
    const response = await createCategory(req.body);
    return res.status(200).json(response);
  } catch (error) {
    return res.status(401).json({
      message: error,
    });
  }
};
const getDetailCategoryController = async (req, res) => {
  try {
    const categoryID = req.params.id;
    const response = await getDetailCategory(categoryID);
    return res.status(200).json(response);
  } catch (error) {
    return res.status(401).json({
      message: error,
    });
  }
};
const getAllCategoryController = async (req, res) => {
  try {
    const response = await getAllCategory();
    return res.status(200).json(response);
  } catch (error) {
    return res.status(401).json(error);
  }
};
const deleteCategoryController = async (req, res) => {
  try {
    const categoryID = req.params.id;
    const response = await deleteCategory(categoryID);
    return res.status(200).json(response);
  } catch (error) {
    return res.status(401).json(error);
  }
};
const updateCategoryController = async (req, res) => {
  try {
    const categoryID = req.params.id;
    const data = req.body;
    const response = await updateCategory(categoryID, data);
    return res.status(200).json(response);
  } catch (error) {
    return res.status(401).json(error);
  }
};
export {
  createCategoryController,
  getDetailCategoryController,
  getAllCategoryController,
  deleteCategoryController,
  updateCategoryController,
};
