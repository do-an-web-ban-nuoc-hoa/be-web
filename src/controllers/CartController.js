import {
  createCart,
  deleteCart,
  deleteCartDetail,
  getCartByUser,
  updateCart,
} from "../services/CartService.js";

const createCartController = async (req, res) => {
  try {
    const data = req.body;
    const response = await createCart(data);
    return res.status(200).json(response);
  } catch (error) {
    return res.status(404).json(error);
  }
};
const updateCartController = async (req, res) => {
  try {
    const data = req.body;
    const cartID = req.params.id;

    const response = await updateCart(data, cartID);
    return res.status(200).json(response);
  } catch (error) {
    return res.status(401).json({
      message: error,
    });
  }
};
const deleteCartController = async (req, res) => {
  try {
    const cartID = req.params.id;
    const response = await deleteCart(cartID);
    return res.status(200).json(response);
  } catch (error) {
    return res.status(401).json(error);
  }
};
const deleteCartDetailController = async (req, res) => {
  try {
    const cartID = req.params.id;
    const data = req.body;
    console.log(data);
    const response = await deleteCartDetail(cartID, data);
    return res.status(200).json(response);
  } catch (error) {
    return res.status(401).json({
      message: error,
    });
  }
};
const getCartByUserController = async (req, res) => {
  try {
    console.log("vào");

    const userID = req.query;
    console.log(userID);
    const response = await getCartByUser(userID);
    return res.status(200).json(response);
  } catch (error) {
    return res.status(401).json(error);
  }
};
export {
  createCartController,
  updateCartController,
  deleteCartController,
  getCartByUserController,
  deleteCartDetailController,
};
