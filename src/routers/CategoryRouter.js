import { Router } from "express";
import {
  createCategoryController,
  getDetailCategoryController,
  getAllCategoryController,
  deleteCategoryController,
  updateCategoryController,
} from "../controllers/CategoryController.js";
const CategoryRouter = Router();

CategoryRouter.post("/create-category", createCategoryController);
CategoryRouter.get("/get-detail-category/:id", getDetailCategoryController);
CategoryRouter.get("/get-all-category", getAllCategoryController);
CategoryRouter.delete("/delete-category/:id", deleteCategoryController);
CategoryRouter.put("/update-category/:id", updateCategoryController);
export default CategoryRouter;
