import { Router } from "express";
import {
  createCartController,
  deleteCartController,
  deleteCartDetailController,
  getCartByUserController,
  updateCartController,
} from "../controllers/CartController.js";
import { authMiddleWare } from "../middleware/authMiddleware.js";

const CartRouter = Router();

CartRouter.post("/create-cart", createCartController);
CartRouter.put("/update-cart/:id", updateCartController);
CartRouter.delete("/delete-cart/:id", deleteCartController);
CartRouter.get("/get-cart", getCartByUserController);
CartRouter.put("/delete-cart-detail/:id", deleteCartDetailController);
export default CartRouter;
