import { Router } from "express";
import {
  createOrderController,
  getReturnUrlOrderController,
} from "../controllers/OrderController.js";

const OrderRouter = Router();

OrderRouter.post("/create-order", createOrderController);
OrderRouter.get("/vnpay_return", getReturnUrlOrderController);
export default OrderRouter;
