import UserRouter from "./UserRouter.js";
import express from "express";
import ProductRouter from "./ProductRouter.js";
import CategoryRouter from "./CategoryRouter.js";
import productImport from "./ImportProductRouter.js";
import BannerRouter from "./BannerRouter.js";
import CartRouter from "./CartRouter.js";
import OrderRouter from "./OrderRouter.js";

const routes = (app) => {
  app.use("/api/user", UserRouter);
  app.use("/api/product", ProductRouter);
  app.use("/api/category", CategoryRouter);
  app.use("/api/import", productImport);
  app.use("/api/banner", BannerRouter);
  app.use("/api/cart", CartRouter);
  app.use("/api/order", OrderRouter);
};
export default routes;
