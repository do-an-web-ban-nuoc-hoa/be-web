import express from "express";
import {
  createUserController,
  loginUserController,
  updateUserController,
  deleteUserController,
  getAllUserController,
  getDetailUserController,
  refreshTokenController,
} from "../controllers/UserController.js";
import { authMiddleWare } from "../middleware/authMiddleware.js";
const UserRouter = express.Router();

UserRouter.post("/sign-up", createUserController);
UserRouter.post("/sign-in", loginUserController);
UserRouter.put("/update-user/:id", updateUserController);
UserRouter.delete("/delete-user/:id", authMiddleWare, deleteUserController);
UserRouter.get("/getAll", getAllUserController);
UserRouter.get("/get-detail/:id", getDetailUserController);
UserRouter.post("/refresh-token", refreshTokenController);
export default UserRouter;
