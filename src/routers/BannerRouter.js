import { Router } from "express";
import {
  createBannerController,
  getAllBannerController,
} from "../controllers/BannerController.js ";

const BannerRouter = Router();
BannerRouter.get("/get-all-banner", getAllBannerController);
BannerRouter.post("/create-banner", createBannerController);
export default BannerRouter;
