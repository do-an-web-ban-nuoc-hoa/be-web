import express from "express";
import multer from "multer";
import path from "path";
import bodyParser from "body-parser";
import {
  createProductController,
  getAllProductController,
  getDetailProductController,
  updateProductController,
  deleteProductController,
  deleteAllProductController,
} from "../controllers/ProductController.js";
const ProductRouter = express.Router();
ProductRouter.post("/create-product", createProductController);
ProductRouter.get("/getAll-product", getAllProductController);
ProductRouter.get("/get-detail-product/:id", getDetailProductController);
ProductRouter.post("/update-product/:id", updateProductController);
ProductRouter.delete("/delete-product/:id", deleteProductController);
ProductRouter.delete("/delete-all-product", deleteAllProductController);

export default ProductRouter;
