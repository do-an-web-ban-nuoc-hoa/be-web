import multer from "multer";
import path from "path";
import bodyParser from "body-parser";
import express from "express";
import { fileURLToPath } from "url";
import { ProductImportController } from "../controllers/ProductController.js";
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const productImport = express();

productImport.use(bodyParser.urlencoded({ extended: true }));
productImport.use(express.static(path.resolve(__dirname, "public")));

var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./src/public/uploads");
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  },
});

var upload = multer({ storage: storage });

productImport.post(
  "/import-product",
  upload.single("file"),
  ProductImportController
);
export default productImport;
